#!/usr/bin/bash

sample=$1
tumour=$2
tumour_bam=$3
script_dir=$4
pathdir=$5
samtools_path=$6
databases=$7

#a) Trabajo con el TUMOUR
#a2) Trabajo con los OAE-TE

#$samtools_path/samtools view -L ${databases}/chrs_of_interest.bed -F1792 $tumour_bam | awk 'BEGIN {OFS="\t"} {if (($2 == 81) || ($2 == 161) || ($2 == 97) || ($2 == 145) || ($2 == 65) || ($2 == 129) || ($2 == 113) || ($2 == 177)) {print $0}}' > $pathdir/$sample/$tumour/$tumour.dis.sam
$samtools_path/samtools view -F1792 $tumour_bam | awk 'BEGIN {OFS="\t"} {if (($"2" == 81) || ($"2" == 161) || ($"2" == 97) || ($"2" == 145) || ($"2" == 65) || ($"2" == 129) || ($"2" == 113) || ($"2" == 177)) {print $"0"}}' > $pathdir/$sample/$tumour/$tumour.dis.sam

awk 'OFS="\t" {print$1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' $pathdir/$sample/$tumour/$tumour.dis.sam > $pathdir/$sample/$tumour/$tumour.dis.abr.sam

sort -k1 $pathdir/$sample/$tumour/$tumour.dis.abr.sam > $pathdir/$sample/$tumour/$tumour.dis.abr.ok.sam

#rm $pathdir/$sample/$tumour/$tumour.dis.abr.sam

perl $script_dir/scripts/make1from2_TE.pl $pathdir/$sample/$tumour/$tumour.dis.abr.ok.sam > $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.sam


#rm $pathdir/$sample/$tumour/$tumour.dis.abr.ok.sam

awk 'OFS="\t" {if (($3 ne $13) && ($15 != 0) && ($5 == 0)){print $1,$13,$14,$10,$12}}' $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.sam > $pathdir/$sample/$tumour/$tumour.chrA.sam

awk 'OFS="\t" {if (($3 ne $13) && ($15 == 0) && ($5 != 0)){print $1,$3,$4,$20,$2}}' $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.sam > $pathdir/$sample/$tumour/$tumour.chrB.sam


cat $pathdir/$sample/$tumour/$tumour.chrA.sam $pathdir/$sample/$tumour/$tumour.chrB.sam > $pathdir/$sample/$tumour/$tumour.chr.sam

awk 'OFS="\t" {if (($5 == 161) || ($5 == 97) || ($5 == 65) || ($5 == 129)) {print $1,$2,$3,$4,"+"}}' $pathdir/$sample/$tumour/$tumour.chr.sam > $pathdir/$sample/$tumour/$tumour.chrpos.sam

awk 'OFS="\t" {if (($5 == 81) || ($5 == 145) || ($5 == 113) || ($5 == 177)) {print $1,$2,$3,$4,"-"}}' $pathdir/$sample/$tumour/$tumour.chr.sam > $pathdir/$sample/$tumour/$tumour.chrneg.sam

#rm $pathdir/$sample/$tumour/$tumour.chrA.sam

#rm $pathdir/$sample/$tumour/$tumour.chrB.sam

#rm $pathdir/$sample/$tumour/$tumour.chr.sam

#a3) UNION

cp $pathdir/$sample/$tumour/$tumour.chrpos.sam $pathdir/$sample/$tumour/$tumour.plus_reads.txt


cp $pathdir/$sample/$tumour/$tumour.chrneg.sam $pathdir/$sample/$tumour/$tumour.minus_reads.txt

awk '{print">"$1"\n"$4}' $pathdir/$sample/$tumour/$tumour.plus_reads.txt > $pathdir/$sample/$tumour/$tumour.plus_reads.tr.fa

awk '{print">"$1"\n"$4}' $pathdir/$sample/$tumour/$tumour.minus_reads.txt > $pathdir/$sample/$tumour/$tumour.minus_reads.tr.fa

#----
cat $pathdir/$sample/$tumour/$tumour.plus_reads.tr.fa $pathdir/$sample/$tumour/$tumour.minus_reads.tr.fa > $pathdir/$sample/$tumour/$tumour.plusandminus_reads.tr.fa
sleep 20
#rm $pathdir/$sample/$tumour/$tumour.plus_reads.tr.fa
#rm $pathdir/$sample/$tumour/$tumour.minus_reads.tr.fa
