#!/usr/bin/perl -w

use strict;

if (scalar(@ARGV) != 4){
        print "Incorrect number of files\n";
}

my $infile1 = $ARGV[0];
my $infile2 = $ARGV[1];
my $pathdir = $ARGV[2];
my $sample = $ARGV[3];

my $outfile1 = "$pathdir/$sample.TEs_ci";#ci means complete info

open(FILE1, "<$infile1") || die("Couldn't open $infile1: $!\n");
my @file1_lines = <FILE1>;
close(FILE1);

open(FILE2, "<$infile2") || die("Couldn't open $infile2: $!\n");

open(OUT1, ">$outfile1") || die("Couldn't open $outfile1\n");

	while  (my $line2 = <FILE2>){
		chomp $line2;
		my @aline2 = split('\t', $line2);
		my $file2_chr = $aline2[0];
		my $file2_lowerlimit = $aline2[1];
		my $file2_upperlimit = $aline2[2];
		my $file2_numberofreads = $aline2[3];
		my $file2_TEfam = $aline2[4];
		my $file2_readsID = $aline2[5];
		for my $i (0..$#file1_lines){
			chomp $file1_lines[$i];
			my ($file1_chr,$file1_lowerlimit,$file1_upperlimit,$file1_numberofreads,$file1_TEfam,$file1_readsID) = split("\t", $file1_lines[$i]);
			
			
			if (($file1_chr eq $file2_chr) && ($file2_lowerlimit >= $file1_upperlimit) && (abs($file2_lowerlimit - $file1_upperlimit) <= 200) && ($file1_TEfam eq $file2_TEfam)) {

				print(OUT1 "$file1_lines[$i]\t$line2\n");

			}
		}	
	}

close(FILE1);
close(FILE2);

unless (-e $outfile1)
{
  warn "file $outfile1 is missing";
  exit(1)
}


