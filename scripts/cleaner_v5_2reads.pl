#!/usr/bin/perl
use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use Utils;

#Igual que /nfs/users/nfs_j/jt14/Desktop/cleaner_v5.pl pero con filtro en 2 reads en lugar de 3 reads;

if (scalar(@ARGV) != 2){
        print "Incorrect number of inputs";
        exit -1;
}

my $infile1 = $ARGV[0];#tumor
my $infile2 = $ARGV[1];#normal


open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");
my @file1_lines = <FILE1>;
chomp(@file1_lines);
close(FILE1);


open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");
my @file2_lines = <FILE2>;
chomp(@file2_lines);
close(FILE2);



my $chr2data = slice_by_chr(\@file2_lines);


foreach my $line1 (@file1_lines){
    my $flag='';
        
	my ($T_chr,$T_pos1L,$T_pos1U,undef,$T_tefam,undef,undef,$T_pos2L,$T_pos2U,undef,undef,undef) = split('\t',$line1);
	
	#print "|${T_chr}|->$line1\n";
	#print $T_chr,undef,$T_pos1,undef,undef,undef,undef,$T_pos2"
    
    if (not exists $chr2data->{$T_chr})
	{
		print $line1,"\n";
		next;
	}
    my @chr_data = @{$chr2data->{$T_chr}};
	foreach my $line2 (@chr_data){
		chomp $line2;
		my ($N_chr,$N_pos1L,$N_pos1U,$N_reads,$N_tefam,undef) = split('\t',$line2);
		if (($T_chr eq $N_chr) && ($T_tefam eq $N_tefam))
		{
			if (($N_reads >= 2) && ((abs($N_pos1U - $T_pos1U) <= 200) || (abs($N_pos1L - $T_pos2L) <= 200)))
			{
				#print "here";
				$flag = 'no_interesa';
				last; 
			}
		}
	}
	if($flag ne 'no_interesa'){ print $line1,"\n";}
}