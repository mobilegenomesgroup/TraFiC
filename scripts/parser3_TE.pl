use strict;

my $preline;
my $current;
my $prevline = 0;

my $incluster = 1; 
my @cluster;


while(<>){
    chomp $_;

    if($. - $prevline == 1){ 
       $preline = $current;
    }
    $current = $_;
    $prevline++;

    if($. == 1){next;}     
    
    if (juntar($preline,$current)){ 
      push(@cluster,($preline,$current));
    }
    
    elsif(not juntar($preline,$current) ){
        my @final = finalize(@cluster);
        print (join("\t",@final),"\n") if scalar @cluster != 0;
        @cluster=();
       
    }
	if (eof) {
	     my @final = finalize(@cluster);
             print (join("\t",@final),"\n") if scalar @cluster != 0;
        }    
    
}


sub juntar{
   my ($preline,$current) = @_;
   my($pread,$pchr,$ppos,$pTEfamily,$pconting) = split("\t",$preline);
   my($cread,$cchr,$cpos,$cTEfamily,$cconting) = split("\t",$current);
   

   if( ($pchr eq $cchr) && (($cpos - $ppos) <=200) && ($pTEfamily eq $cTEfamily) ){
       return(1);
   } 
   else{
     return(0);
   }
}

sub finalize{
   my @cluster = @_;
   my $first = $cluster[0];
   my $last = $cluster[scalar @cluster - 1];
   my ($fread,$fchr,$fpos,$fTEfamily,$fconting) = split("\t",$first);
   my ($lread,$lchr,$lpos,$lTEfamily,$lconting) = split("\t",$last);
   
   my %unique;
   my @reads; 
   foreach(@cluster){
       my ($read) = split("\t",$_);
       $unique{$read}++; 
   }
   
   my $reads = join(',',keys %unique);
   return($fchr,$fpos,$lpos,scalar keys %unique,$fTEfamily,$reads); 
}

