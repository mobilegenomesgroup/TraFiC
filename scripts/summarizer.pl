#!/usr/bin/perl -w
use strict;
use warnings;

#una especie de make1from2, pero lo que hace es abreviar el archivo con el total de predicciones en TraFiCdb a un archivo no redundante;

if (scalar(@ARGV)!=1)
{
	print "ERROR: there must be one input file\n";
	exit -1
}

my $infile = $ARGV[0];

open (FILE,"<$infile");

my $preline = "";
my $nline = 0;

my @lineas = ();
my @sample = (); 


#my @contador = ();

my $comodin1 = ();
my $comodin2 = ();

	while (my $line = <FILE>) 
	{
		chomp $line;
	
		if ($nline > 0)
		{
#			@tmp = ();
#			$tmp[0] = $preline;
			my @tmp0 = split('\t',$preline);
#			$tmp[1] = $line;
			my @tmp1 = split('\t',$line);
			
			if (($tmp0[0] eq $tmp1[0]) && (abs($tmp1[2] - $tmp0[2]) <= 600) && (abs($tmp1[7] - $tmp0[7]) <= 600) &&  ($tmp0[4] eq $tmp1[4])  ){

				if (scalar @lineas == 0){

					push (@lineas, $preline);
					push (@lineas, $line);
					push (@sample, $tmp0[12]);
					push (@sample, $tmp1[12]); 

				}else{
					
					push (@lineas, $line);
					push (@sample, $tmp1[12]);

				}

#				$comodin1 = ("$tmp1[12]");

#				print("PRELINE:@tmp0\nLINE:@tmp1\n------------------\n");
			}else{
				if ((scalar @lineas > 0)){

					my $total_samples = join (',',@sample);
					my $number_samples = scalar @sample;

					print ("$lineas[0]\t$total_samples\t$number_samples\n");

					@lineas = ();
					@sample = ();

				}else{

					$comodin1 = $line;
					$comodin2 = $tmp1[12];
					print ("$preline\t$tmp0[12]\t1\n");

				}

			}
			
		}
		
		$nline ++;
		$preline = $line;
		if (eof){

			if (scalar @lineas > 0){

			my $total_samples = join (',',@sample);
			my $number_samples = scalar @sample;

			print ("$total_samples\n");
			print ("$lineas[0]\t$total_samples\t$number_samples\n");

		}else{

			print ("$comodin1\t$comodin2\t1\n");

		}

		}

	}
close FILE;

	
