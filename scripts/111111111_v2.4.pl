#!/usr/bin/perl -w

use strict;
use FindBin '$Bin';

if (scalar(@ARGV) != 2){
        print "Incorrect number of files\n";
}

my $pathdir = $ARGV[0];
my $sample = $ARGV[1];

system ("perl $Bin/tubiosort.pl $pathdir/$sample.1111.txt > $pathdir/$sample.1111.2.txt");


my $infile1 = "$pathdir/$sample.1111.2.txt";
my $infile2 = "$pathdir/filter3.td0.4C.txt";
my $infile3 = "$pathdir/filter3.td1.4C.txt";
my $infile4 = "$pathdir/filter3.td2.4C.txt";

my $outfile1 = "$pathdir/1112.intermediate.txt";


open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");
open(OUT1, ">$outfile1") or die("Couldn't open $outfile1: $!\n");


my $header = "#Chromosome\tPositive_breakpoint\tNegative_breakpoint\tTE_type\tGene\tStatus\tSource_element_chromosome\tSource_positive_breakpoint\tSource_negative_breakpoint\tSource_strand\tTransduction_breakpoint_1\tTransduciton_breakpoint_2\tDistance_from_source_copy_to_transduction\ttransduction_size\tSupporting_reads_positive_breakpoint\tSupporting_reads_negative_breakpoint";
print (OUT1 "$header\n");

while (my $line1 = <FILE1>){
	$line1 =~ s/\n//g;
	my @aline1 = split('\t', $line1);
	my $new_pos = $aline1[2]+100;
	open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");
	while (my $line2 = <FILE2>){
		$line2 =~ s/\n//g;
		my @aline2 = split('\t', $line2);

		if (($aline1[6] eq "td0") && ($aline1[1] ne "MT") && ($aline1[1] ne "Y") && ($aline1[1] !~ /hs/) && ((($aline2[2] - $aline1[3]) < 500) && (($aline2[2] - $aline1[3]) > -500)))  {

			print (OUT1 "$line1\t$aline2[3]\t$aline2[9]\n");

		}
	}


	open(FILE3, "<$infile3") or die("Couldn't open $infile3: $!\n");
	while (my $line3 = <FILE3>){
		$line3 =~ s/\n//g;
		my @aline3 = split('\t', $line3);

		if (($aline3[1] ne "MT") && ($aline3[1] ne "Y") && ($aline3[1] !~ /hs/) && ((($aline3[2] - $aline1[3]) < 500) && (($aline3[2] - $aline1[3]) > -500)))  {
			print (OUT1 "$line1\t$aline3[3]\t$aline3[9]\n");
		}
	}


	open(FILE4, "<$infile4") or die("Couldn't open $infile4: $!\n");
	while (my $line4 = <FILE4>){
		$line4 =~ s/\n//g;
		my @aline4 = split('\t', $line4);

		if (($aline4[1] ne "MT") && ($aline4[1] ne "Y") && ($aline4[1] !~ /hs/) && ((($aline4[2] - $aline1[3]) < 500) && (($aline4[2] - $aline1[3]) > -500)))  {
			print (OUT1 "$line1\t$aline4[3]\t$aline4[9]\n");

		}
	}


}

close(OUT1);

open (OUT1, "<$outfile1") or die("Couldn't open $outfile1: $!\n");
my $outfile2 = "$pathdir/trafic_mem_output.txt";
open (OUT2, ">$outfile2") or die("Couldn't open $outfile2: $!\n");

my $preline = "";
my $nline = 0;

my @array_genes = ();

        while (my $line = <OUT1>) {
                chomp $line;

		if ($nline > 0) {

		my @array_prev = split('\t', $preline);
		my $prev_id_pos2 = $array_prev[3];
		my $prev_id_gene = $array_prev[5];

		my @array_line = split('\t', $line);
		my $line_id_pos2 = $array_line[3];
		my $line_id_gene = $array_line[5];

#		print (OUT2 "PRELINE:$prev_id_pos2,$prev_id_gene----LINE:$line_id_pos2,$line_id_gene\n");

		if ($prev_id_pos2 ne $line_id_pos2){

			if (@array_genes == 0){

				print(OUT2 "$preline\n");

			}else{
				my @only_genes = ();
				foreach (@array_genes){
					if ($_ ne "-"){
						push(@only_genes, $_);
					}
				}

				if (@only_genes == 0){

					my $list_of_genes = join(',', @array_genes);
					my $array_prev_firstpart = join ("\t", @array_prev[0..4]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..17]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..16]);
					my $array_prev_secondpart = join ("\t", @array_prev[6 .. $#array_prev]);
					
					print(OUT2 "$array_prev_firstpart\t$list_of_genes\t$array_prev_secondpart\n");
					@array_genes = ();

				}else{

					my $list_of_onlygenes = join(',', @only_genes);
					my $array_prev_firstpart = join ("\t", @array_prev[0..4]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..17]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..16]);
					my $array_prev_secondpart = join ("\t", @array_prev[6 .. $#array_prev]);
					
					print(OUT2 "$array_prev_firstpart\t$list_of_onlygenes\t$array_prev_secondpart\n");
					@array_genes = ();
					@only_genes = ();

				}
			}

		}else{


			if (@array_genes == 0){

				push(@array_genes, $prev_id_gene);	
#				print (OUT2 "@array_genes\n");

			}else{
				if ($prev_id_gene ne $line_id_gene){
					push(@array_genes, $line_id_gene);
#					print(OUT2 "@array_genes\n");

				}else{
					0;
#					print(OUT2 "@array_genes\n");
				}

			}
				
		}

		if (eof){

			if (@array_genes == 0){
				print(OUT2 "$line\n");
			}else{

				my @only_genes = ();
				foreach (@array_genes){
					if ($_ ne "-"){
						push(@only_genes, $_);
					}
				}

				if (@only_genes == 0){
					my $list_of_genes = join(',', @array_genes);
					my $array_prev_firstpart = join ("\t", @array_prev[0..4]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..17]);
					my $array_prev_secondpart = join ("\t", @array_prev[6 .. $#array_prev ]);
					print(OUT2 "$array_prev_firstpart\t$list_of_genes\t$array_prev_secondpart\n");
					@array_genes = ();

				}else{
					my $list_of_onlygenes = join(',', @only_genes);
					my $array_prev_firstpart = join ("\t", @array_prev[0..4]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..17]);
					#my $array_prev_secondpart = join ("\t", @array_prev[6..16]);
					my $array_prev_secondpart = join ("\t", @array_prev[6 .. $#array_prev ]);
					
					print(OUT2 "$array_prev_firstpart\t$list_of_onlygenes\t$array_prev_secondpart\n");
					@array_genes = ();
					@only_genes = ();

				}


			}


		}

		}
                
                $nline ++;
                $preline = $line;


	}



