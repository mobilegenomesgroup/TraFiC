#!/usr/bin/bash

sample=$1
normal=$2
script_dir=$3
pathdir=$4

perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_Alu_mas.ok.txt > $pathdir/$sample/$normal/clusters_Alu_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_L1_mas.ok.txt > $pathdir/$sample/$normal/clusters_L1_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_SVA_mas.ok.txt > $pathdir/$sample/$normal/clusters_SVA_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_ERVK_mas.ok.txt > $pathdir/$sample/$normal/clusters_ERVK_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_mas.ok.PolyA.txt > $pathdir/$sample/$normal/clusters.PolyA.mas.txt

perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_Alu_menos.ok.txt > $pathdir/$sample/$normal/clusters_Alu_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_L1_menos.ok.txt > $pathdir/$sample/$normal/clusters_L1_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_SVA_menos.ok.txt > $pathdir/$sample/$normal/clusters_SVA_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_ERVK_menos.ok.txt > $pathdir/$sample/$normal/clusters_ERVK_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$normal/MASKEDreads_menos.ok.PolyA.txt > $pathdir/$sample/$normal/clusters.PolyA.menos.txt

#rm $pathdir/$sample/$normal/MASKEDreads_mas.ok.PolyA.txt
#rm $pathdir/$sample/$normal/MASKEDreads_menos.ok.PolyA.txt

#rm $pathdir/$sample/$normal/MASKEDreads_Alu_mas.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_L1_mas.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_SVA_mas.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_ERVK_mas.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_Alu_menos.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_L1_menos.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_SVA_menos.ok.txt
#rm $pathdir/$sample/$normal/MASKEDreads_ERVK_menos.ok.txt

#se suman los clusters;

cat $pathdir/$sample/$normal/clusters_*_mas.txt > $pathdir/$sample/$normal/clusters_mas.txt
cat $pathdir/$sample/$normal/clusters_*_menos.txt > $pathdir/$sample/$normal/clusters_menos.txt
cat $pathdir/$sample/$normal/clusters.PolyA.mas.txt $pathdir/$sample/$normal/clusters.PolyA.menos.txt > $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt

#rm $pathdir/$sample/$normal/clusters_Alu_mas.txt
#rm $pathdir/$sample/$normal/clusters_L1_mas.txt
#rm $pathdir/$sample/$normal/clusters_SVA_mas.txt
#rm $pathdir/$sample/$normal/clusters_ERVK_mas.txt
#rm $pathdir/$sample/$normal/clusters_Alu_menos.txt
#rm $pathdir/$sample/$normal/clusters_L1_menos.txt
#rm $pathdir/$sample/$normal/clusters_SVA_menos.txt
#rm $pathdir/$sample/$normal/clusters_ERVK_menos.txt
