import sys
import csv
import subprocess
from subprocess import Popen, PIPE

fh = open(sys.argv[1],'r')
bed = sys.argv[2]
tabix_path=sys.argv[3]

for row in csv.reader(fh, delimiter='\t'):
	if len(row)>=8:
		left_seen = None;
		right_seen = None;

		#print "MGE4C: start coordinate"
		#print("{}\t{}\t{}\t{}".format(row[0],row[2],row[2],row[4]))
		command = tabix_path+'/'+'tabix '+ bed + ' ' + str(row[0])+':'+str(int(row[1]))+'-'+str(int(row[2]));
		#print command		
		ret = subprocess.check_output(command, shell=True)		
		ret = ret.decode("utf-8")
		#print('returning from tabix')		
		#print ret
		if  ret  and row[4].lower() in ret.lower():
			left_seen = True
			#print('SEEN_LEFT.. '+ row[4].lower() + ' type match ' +ret.lower())			
		
		#print "TRAFIC: end coordinate"	
		#print("{}\t{}\t{}\t{}".format(row[0],row[7],row[7],row[4]))
		command = tabix_path+'/'+'tabix  '+  bed + ' ' + str(row[0])+':'+str(int(row[7]))+'-'+str(int(row[7]));
		#print command
		ret = subprocess.check_output(command, shell=True)
		ret = ret.decode("utf-8")
		#print('returning from tabix')		
		#print ret		
		if  ret  and row[4].lower() in ret.lower():
			#print('SEEN.. '+ row[4].lower() + ' type match ' +ret.lower())			
			right_seen = True	
		
		if not left_seen and not right_seen:
			print ('\t'.join(row))
		#print('______________________')		
		#exit(1)

