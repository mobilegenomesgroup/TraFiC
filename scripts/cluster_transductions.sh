#!/usr/bin/bash

sample=$1
tumour=$2
script_dir=$3
pathdir=$4

#CLUSTERING FOR TRANSDUCTIONS

perl $script_dir/scripts/parser_transductions.pl $pathdir/$sample/$tumour/$tumour.chrpos.td.sam > $pathdir/$sample/$tumour/preclusters_mas.td.txt

perl $script_dir/scripts/parser_transductions.pl $pathdir/$sample/$tumour/$tumour.chrneg.td.sam > $pathdir/$sample/$tumour/preclusters_menos.td.txt

#rm $pathdir/$sample/$tumour/$tumour.chrpos.td.sam
#rm $pathdir/$sample/$tumour/$tumour.chrneg.td.sam

perl $script_dir/scripts/cleaner_v5_forL1andPolyA.pl $pathdir/$sample/$tumour/preclusters_mas.td.txt $pathdir/$sample/$tumour/clusters.PolyA.mas.txt > $pathdir/$sample/$tumour/clusters_mas.td.txt

perl $script_dir/scripts/cleaner_v5_forL1andPolyA.pl $pathdir/$sample/$tumour/preclusters_menos.td.txt $pathdir/$sample/$tumour/clusters.PolyA.menos.txt > $pathdir/$sample/$tumour/clusters_menos.td.txt

#rm $pathdir/$sample/$tumour/preclusters_mas.td.txt
#rm $pathdir/$sample/$tumour/preclusters_menos.td.txt

awk '{if (($4 >= 4) && ($1 ne "MT") && ($1 ne "Y") && ($1 !~ /hs/) && ($1 !~ /GL/)) {print$0}}' $pathdir/$sample/$tumour/clusters_mas.td.txt > $pathdir/$sample/$tumour/clusters_mas.td.4C.txt

awk '{if (($4 >= 4) && ($1 ne "MT") && ($1 ne "Y") && ($1 !~ /hs/) && ($1 !~ /GL/)) {print$0}}' $pathdir/$sample/$tumour/clusters_menos.td.txt > $pathdir/$sample/$tumour/clusters_menos.td.4C.txt
echo "perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.td.4C.txt $pathdir/$sample/$tumour/clusters_menos.4C.txt $pathdir/$sample/$tumour $tumour.tdplus4C"
perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.td.4C.txt $pathdir/$sample/$tumour/clusters_menos.4C.txt $pathdir/$sample/$tumour $tumour.tdplus4C


echo "perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.4C.txt $pathdir/$sample/$tumour/clusters_menos.td.4C.txt $pathdir/$sample/$tumour $tumour.tdminus4C"
perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.4C.txt $pathdir/$sample/$tumour/clusters_menos.td.4C.txt $pathdir/$sample/$tumour $tumour.tdminus4C

echo "perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.td.4C.txt $pathdir/$sample/$tumour/clusters_menos.td.4C.txt $pathdir/$sample/$tumour $tumour.chr2chr4C"
perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.td.4C.txt $pathdir/$sample/$tumour/clusters_menos.td.4C.txt $pathdir/$sample/$tumour $tumour.chr2chr4C

#rm $pathdir/$sample/$tumour/clusters_mas.4C.txt
#rm $pathdir/$sample/$tumour/clusters_menos.4C.txt
#rm $pathdir/$sample/$tumour/clusters_mas.td.4C.txt
#rm $pathdir/$sample/$tumour/clusters_menos.td.4C.txt
