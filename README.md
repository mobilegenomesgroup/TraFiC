TraFiC-mem (Transposon Finder in Cancer)
=====

##### TraFiC-mem uses paired-end sequencing aligned reads for the identification of somatic insertion of transposable elements.
###### Current species supported are:
###### Human GRCh37 (hg19)

---

### running TraFiC-mem
TraFiC is run through a [docker](https://hub.docker.com/r/mobilegenomes/trafic/) container.

#### pre-requisites
[docker](https://www.docker.com/)

#### pull image
````
docker pull mobilegenomes/trafic:multispecies
````

#### create and run the container 
When creating the container you need to mount two folders from the host where the container is created:
* The container's `/trafic_input` folder will mount the host folder containing TraFiC-mem input files. Those files will be the tumour and normal bam files plus the configuration yaml file. Notice that the host folder mounted on `/trafic_input` is the same as in the `tumour_bam` and `normal_bam` keys of the yaml configuration file
* The container's `/trafic_output` folder will mount the host folder where TraFiC-mem will create the output files and folders.

There is a yaml configuration template file in the `workflows/` folder

`config.yaml`
````
user_id: user_1 
project: my_project_name
sample_name: invividual_a
tumour_name: individual_a_tumour
tumour_bam: /trafic_input/xxx.bam (just replace your path to the tumour bam file by '/trafic_input' )
normal_name: individual_a_normal
normal_bam: /trafic_input/xxx.bam (just replace your path to the normal bam file by '/trafic_input' )
output_dir: /full_path_to_existing_output_directory
bwa_threads: number_of_threads_to_run_bwa (i.e. 1)
````

example for human:
````
docker run --volume /path_to_trafic_input_files:/trafic_input --volume /path_to_trafic_output:/trafic_output --name trafic -dt mobilegenomes/trafic:multispecies
docker exec trafic /usr/bin/snakemake --snakefile /trafic/workflows/SnakefileTraficHg19 --configfile /trafic_input/config.yaml --printshellcmds --jobs --verbose 
````

example for human (udocker):
````
udocker create --name=trafic mobilegenomes/trafic:multispecies
udocker run --volume=/path_to_trafic_input:/trafic_input --volume=/path_to_trafic_output:/trafic_output  trafic /usr/bin/snakemake --snakefile /trafic/workflows/SnakefileTraficHg19 --configfile /trafic_input/config.yaml --printshellcmds --jobs --verbose
````



### CPU and memory requirements
TraFiC-mem takes about 12 CPU hours and 8Gb of RAM (we recommend 15Gb) to analyse a 40X whole sequence human genome on 1 CPU.

### interpreting output: 
The output file to inspect is `output_path/sample_name/sample_name.TraFiC.vcf`
	
### test dataset:
The TraFiC-mem installation comes with a human hg19 test dataset composed of a tumour bam file `dataset/tumour.bam` a normal bam file `dataset/normal.bam`  and the expected TraFiC-mem final output `dataset/sample.TraFiC.vcf`  

### contact
mobilegenomes <info@mobilegenomes.tech>

### licence
TraFiC-mem is free software

### What citation should I use for the software TraFiC?
Please use the following reference: Tubio JMC et al. Mobile DNA in cancer. Extensive transduction of nonrepetitive DNA mediated by L1 retrotransposition in cancer genomes. Science. 2014 Aug 1;345(6196):1251343. doi: 10.1126/science.1251343.
