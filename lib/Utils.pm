sub slice_by_chr
{
	my $file2_lines = shift;
	
	my $chr2data = {};
	foreach my $line (@$file2_lines)
	{
  		my ($chr) = split('\t',$line);
  		push(@{$chr2data->{$chr}},$line);
	}
	return($chr2data);
}
1;
